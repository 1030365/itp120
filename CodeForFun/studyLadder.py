import vlc
import time
from os.path import exists
from threading import Timer

working = True
work_time = 60*int(input('How long (in minutes) do you want your first study session to be? '))
added_time = 60*int(input('How much extra time (in minutes) should be added to each subsequent work session? '))
rest_time = 60*int(input('How long (in minutes) do you want your breaks to be? '))
total_time = 60*int(input('How long (in minutes) do you want to do this study ladder for? '))
alarm = vlc.MediaPlayer("Alarm.mp3")
music = True
p = vlc.MediaPlayer("AlphaWaves.mp3")
myMusic = None
finished = False
if input('Would you like study music? ').upper()[0] == 'Y':
    if input('Type 1 for generic study music (Alpha Waves) or type 2 for custom study music: ')[0] == '2':
        myMusic = input('Input audio file name here: ')
        if exists(myMusic):
            p = vlc.MediaPlayer(myMusic)
        else:
            print('There is no audio file in this folder with that name. Using generic study music instead.')
else:
    music = False

def timeout():
    global working, work_time, added_time, rest_time, total_time, p, alarm, finished
    if working:
        if music:
            p.pause()
        working = False
        alarm.play()
        total_time -= work_time
        if total_time == 0:
            print('Ladder Complete!')
            finished = True
            return None
        t = Timer(rest_time, timeout)
    else:
        working = True
        total_time -= rest_time
        work_time += added_time
        if work_time+rest_time >= total_time:
            work_time = total_time
        t = Timer(work_time, timeout)
        alarm.play()
    t.start()

t = Timer(work_time, timeout)
t.start()
print('Study ladder begins now')
if music:
    p.play()

while True:
    if str(p.get_state())=="State.Ended":
        p.stop()
        p.play()
    if str(alarm.get_state())=="State.Ended":
        alarm.stop()
        if finished:
            break
        if working and music:
            p.play()
