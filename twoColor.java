import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JColorChooser;
import javax.swing.JPanel;

class ShowColors2JFrame extends JFrame {
    private JButton changeColorJButton;
    
    //use RANDOM starting color
    private Color color = new Color((int) (Math.random() * 256),(int) (Math.random() * 256),(int) (Math.random() * 256));
    
    private JPanel colorJPanel;

    //set up GUI
    public ShowColors2JFrame() {
        super("Using JColorChooser");

        //create JPanel for display color
        colorJPanel = new JPanel();
        colorJPanel.setBackground(color);

        //set up changeColorJButton and register its event handler
        changeColorJButton = new JButton("I'm bored");
        changeColorJButton.addActionListener(
                //display JColorChooser when user clicks button
                new ActionListener() {
                    public void actionPerformed(ActionEvent event) {
                        color = JColorChooser.showDialog(ShowColors2JFrame.this, "Choose a color", color);

                        //set RANDOM color, if no color is returned
                        if (color == null) {
                            color = new Color((int) (Math.random() * 256),(int) (Math.random() * 256),(int) (Math.random() * 256));
                        }
                        //change content pane's background color
                        colorJPanel.setBackground(color);
                    }
                }
        );
        //add colorJPanel and button
        add(colorJPanel, BorderLayout.CENTER);
        add(changeColorJButton, BorderLayout.SOUTH);

        //set frame formatting
        setSize(800,600);
        setVisible(true);
    }
}

class ShowColors2 {
    //execute application
    public static void main(String[] args) {
        ShowColors2JFrame application = new ShowColors2JFrame();
        application.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }
}
