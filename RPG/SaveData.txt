geoX: 63
geoY: 40
atkl: 120
atkh: 140
magic_power: 100
def: 120
hp: 250
max_hp: 250
mp: 40
max_mp: 140
exp: 88
max_exp: 110
lvl: 55
bosses_beaten: 2
raft: True
apples: 21
potions: 10
steaks: 3
cupcakes: 20
starbits: 12
dragon_breaths: 3
rocks: 20
shurikens: 12
arrows: 2
world: 
#######                         #                           W                             WWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWW
#######                      ##W                      S     W                             WMMMMMMMMMMMMMMMMMMMMMMMMMMMMWWMMMMMMMMMMMMMMMMMMMMMMMMMMMMW
#######                      ##W                      S     W                             WMMMMM4MMMMMMMMMMMMMMMMMMMMMMWWMMMMMMMMMMMMMMMMMMMMMMMMMMMMW
#1####                       ##W                       S    W                             WMMMMMMMMMMMMMMMMMMMMMMMMMMMMWWMMMMMMMMMMMMMMMMMMMMMMMMMMMMW
#####P                       ##W                        S   W                             WMMMMMMMMMMMMMMMMMMMMMMMMMMMMWWMMMMMMMMMMMMMMMMMMMMIMMMMMMMW
##   P     # #               ##W                         S  W                             WMMMMMMMMMMMMMMMMMMMMMMMMMMMMWWMMMMMMMMMMMMMMMMMMMMMMMMMMMMW
     P     # #               ##W                          S W                             WMMMMMMMMMMMMMMMMMMMMMMMMMMMMWWMMMMMMMMIMMMMMMMMMMMMMMMMMMMW
     P   #     #             ##W                           SW                             WMMMMMMMMMMMMMMMMMMMMMMMMMMMMWWMMMMMMMMMMMMMMMMMMMMMMMMMMMMW
     P    #####              ##W                           SW                             WMMMMMMMMMMMMMMMMMMMMMMMMMMMMWWMMMMMMMMMMMMMMMMMMMMMMMMMMMMW
     P                       ##W                           SW                             WMMMMMMMMMMMMMMMMMMMMMMMMMMMMWWMMMMMMMMMMMMMMMMMMMMMMMMMMMMW
     PPPPP                   ##W                           SW                             WMMMMMMMMMMMMMMMMMMMMMMMMMMMMWWMMMMMMMMMMMMMIMMMMMMMMMMMMMMW
         P                   ##SW                          SW                             WMMMMMMMMMMMMMMMMMMMMMMMMMMMMWWMMMMMMMMMMMMMMMMMMMMMMMMMMMMW
         P  ####             ##SW                          SW                             WMMMMMMMMMMMMIMMMMMMMMMMMMMMMWWMMMMMIMMMMMMMMMMMMMMMMMMMMMMW
         P#########          ##SW                          SWSSS      PPP                 WMMMMMMMMMMMMMMMMMMMMMMMMMMMMWWMMMMMMMMMMMMMMMMMMMMMMMMMMMMW
         ###########         ##SSW        SSSSS            SWSISPP  PPP3P                 WMMMMMMMMM   M   MMMMMMMMMMMMWWMMMMMMMMMMMMMMMMMMMMMMMMMMMMW
        ###W#########        ##SSWW    SSSS   S     SSS    WWSSS      PPP                 WMMMMMMMMM PPPPP MMMMMMMMMMMMWWMMMMMMMMMMMMMMMMMMMMIMMMMMMMW
        ###W#########PPPPPPPP##SSSSSSSSS      S  SSSS SSSSSSSSSS                          WMMMMMMMMM PPPPP MMMMMMMMMIMMWWMMMMMMMMMMMMMMMMMMMMMMMMMMMMW
        ###WWWW######        ##SSSSW          SSSS         WW                             WMMMMMMMMMMPPCPPMMMMMMMMMMMMMWWMMIMMMMMMMMMMM     MMMMMMMMMW
         ###4#W#####         ##SSSSSW                      SW                             WMMMMMMMMM PPPPP MMMMMMMMMMMMWWMMMMMMMMMMMMM       MMMMMMMMW
         ###W#######         ##SS2SSW                      SW                             WMMMMMMIMM PPPPP MMMMMMMMMMMMWWMMMMMMMMMMMM        MMMMMMMMW
          #########          ##SSSSSW                      SW                            #WMMMMMMMMM   M   MMIMMMMMMMMMWWMMMMMMMMMMMM         MMMMMMMW
            ###              ##SSSSW                       SW                           ##WMMMMMMMMMMMMMMMMMMMMMMMMMMMMWWMMMMMMM              MMMMMMMW
                             ##SSSSW                       SW                          WWWWMMMMMM                                             MMMMMMMW
                             ##SSSW                        SW                                                                   MMMMM         MMMMMMMW
                            W##SSSW                        SW                                         MMMMMMMMMMIMMMMMMWWMMMMMMMMMMMMMMMMM  MMMMMMMMMW
                            W##SSW                         SW                          WWWWMMMMMMMMMMMMMMMMMMMMMMMMMMMMWWMMMMMMMMMMMMMMMMM  MMMMMMMMMW
                            W##SW                          SW                           ##WMMMMMMMMMIMMMMMMMMMMMMMMMMMMWWMIMMMMMMMMMMMMMMM   MMMMMMMMW
 ###                        W##SW                          SW                            #WMMMMMMMMMMMMMMMMMMMMMMMMMMMMWWMMMMMMIMMMMMMMMMM   MMMMMMMMW
 ###        WWWWWWWWWWWWWWWWW##W                           SW                             WMMMMMMMMMMMMMMMMMMMMMMMMMMMMWWMMMMMMMMMMMMMMMMMM  MMMMMMMMW
  ##        W##################W                           SW                             WWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWW  WWWWWWWWW
W#    WWW   W##################W                           W                                                            WWWWWWWWWWWWWWWWWWW  WWWWWWWWW
W#    W W   W###1##############W                           W                                                            W      SSSS       W  W       W
W#  WWWWW   W#######I##########W                           W                                                            W      SISSS      W  W       W
W#  W W     W##################W                           W                                                            W      SSSSS      WWWW       W
W#  WWW     W##################WWWWWWWWWWWWWWWWWWWWWWWWWWWWW                                                            W       SSS                  W
W#          W##############################################W                                                            W                            W
W#WWWWWWWWWWW##############################################W  SSSSSSSSS                                                 W                            W
W##########################################################W  SSSSSSSSS                                                 W              SSS           W
W##########################################################W  SSSWPWSSS                                                 W          SSSSSSSS          W
W######################################################2###W  SSSWPWSSS                                                 W          SSISSSSS          W
W###############WWW#WWWWW##################################W  SCSWPWSSS                                                 W           SSSSISS          W
W###############W W#W W W##################################W  SSSWPWSSS                                                 W           SSSSSS           W
W###PPP#########W WWW WWW######################  ##########W  WWWWPWWWW                                                 W                            W
W###PCP#########W     W W#########I#########       ########W  WPIPIPIPW                                                 W                            W
W###PPP#########W WWW W W##################        ########W  WIPIPIPIWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWW                            W
W###############W W#W W W##################       #########W  WPIPIPIPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPP                  SSSS           W
W###############WWW#WWWWW###################      #########W  WIPIPIPIPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPP                SSSS#S           W
W##############################################  ##########W  WPIPIPIPWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWW           SSI##S           W
W##########################################################W  WPPPPPPPW                                                 W           S###SS           W
W##########################################################W  WWWWWWWWW                                                 W           SSSSS            W
W##########################################################W                                                            W       PP                   W
W##########################################################W                                                            W       PP                   W
W##########################################################W                                                            W       PP                   W
W###########################################I##############W                                                            W  SSSSSSSSSSSSSSSSS         W
W##########################################################W                                                            WSSSSSSSSSSSSSSSSSSSSSSSSSSSSW
W##########################################################W                                                            WSSSSS############SSSSSSSSSSSW
W#######I##################################################W                                                            W############################W
W##########################################################W                                                            W##########B######C##########W
W##########################################################W                                                            W############################W
WWWWWWWWWWWWWWW#WWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWW                                                            WWWWWWWWWWWWWWWWWWWWWWWWWWWWWW
               #######                                                                                                                                
               #######                                                                                                                                
               #######PPPPPPPPPPPPPPPPPPP                                                                                                             
                                        P                                                                                                             
                                        P                                                                                                             
                                        P     PPP                                                                                                     
                                        P     P3P                                                                                                     
                                        P     PPP                                                                                                     
                                        P      P                                                                                                      
                                        P      P                                                                                                      
                                       PPP    PPP                                                                                                     
                                       PPPPPPPPCP                                                                                                     
                                       PPP    PPP                                                                                                     
                                                                                                                                                      
                                                                                                                                                      
                                                                                                                                                      
                                                                                                                                                      
                                                                                                                                                      
                                                                                                                                                      
                                                                                                                                                      
                                                                                                                                                      
                                                                                                                                                      
                                                                                                                                                      
                                                                                                                                                      
                                                                                                                                                      
                                                                                                                                                      
                                                                                                                                                      
                                                                                                                                                      
                                                                                                                                                      
                                                                                                                                                     