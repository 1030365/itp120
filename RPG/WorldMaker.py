from gasp import games
from gasp import color
from gasp import boards
from random import randint

BOX_SIZE = 20
MARGIN = 30
WIDTH = 30
HEIGHT = WIDTH
TEXT_SIZE = 20
COMMAND = [1073742051,1073742055]

class WorldMaker(boards.SingleBoard):
    def __init__(self):
        self.tipTimer = -1
        self.chunkcoords = [1,1]
        self.selecting = False
        self.dragging = False
        self.old_click = (0,0)
        self.selected_coords = [[-1,-1],[-1,-1]]
        self.sel_width = 0
        self.sel_height = 0
        self.selected_colors = []
        self.selection = ['']
        self.chunk = []
        self.world = []
        self.makeWorld()
        self.init_singleboard((MARGIN, MARGIN), WIDTH, HEIGHT, BOX_SIZE)
        self.draw_all_outlines()
        self.create_directions()
        self.mouse_pos = (0,0)
        x, y = self.cell_to_coords(WIDTH//2+0.5, HEIGHT//2+0.5)
        self.spawn_indicator = games.Text(self, x, y, "S", round(1.5*BOX_SIZE), color.RED)
        x, y = self.cell_to_coords(WIDTH/2,HEIGHT+0.5)
        self.tip = games.Text(self, x, y, '', TEXT_SIZE, color.RED)
        self.guide = True
        self.tip_display('Press COMMAND + G To Open The User Guide')

    def new_gamecell(self, i, j):
        strg = []
        for w in range(1,10):
            strg.append(str(w))
        if self.chunk[j][i] == "#":
            return Grass(self, i, j)
        elif self.chunk[j][i] == " ":
            return Water(self, i, j)
        elif self.chunk[j][i] == "S":
            return Sand(self, i, j)
        elif self.chunk[j][i] == "W":
            return Wall(self, i, j)
        elif self.chunk[j][i] == "M":
            return Mud(self, i, j)
        elif self.chunk[j][i] == "P":
            return Plank(self, i, j)
        elif self.chunk[j][i] == "C":
            return Checkpoint(self, i, j)
        elif self.chunk[j][i] == "R":
            return Raft(self, i, j)
        elif self.chunk[j][i] == "I":
            return WorldItem(self, i, j)
        elif self.chunk[j][i] == "B":
            return Boss(self, i, j)
        elif self.chunk[j][i] in strg:
            return Teleporter(self, i, j)

    def keypress(self, key):
        strg = []
        for i in range(49, 58):
            strg.append(i)
        if key == boards.K_RIGHT:
            self.loadchunk(self.chunkcoords[0]+1,self.chunkcoords[1])
        elif key == boards.K_LEFT:
            self.loadchunk(self.chunkcoords[0]-1,self.chunkcoords[1])
        elif key == boards.K_DOWN:
            self.loadchunk(self.chunkcoords[0],self.chunkcoords[1]+1)
        elif key == boards.K_UP:
            self.loadchunk(self.chunkcoords[0],self.chunkcoords[1]-1)
        elif key == 114 and self.command():
            self.reset_world()
        elif key == 115 and self.command():
            self.trim()
            self.save_world()
        elif key == 116 and self.command():
            self.trim()
        elif key == 116:
            pass
        elif key == 27 and not self.selecting:
            for i in self.grid:
                for j in i:
                    j.deselect()
        elif key == 114 and not self.command():
            self.replace_tile("R",self.mouse_pos[0],self.mouse_pos[1])
        elif key == 98:
            self.replace_tile("B",self.mouse_pos[0],self.mouse_pos[1])
        elif key == 99:
            self.replace_tile("C",self.mouse_pos[0],self.mouse_pos[1])
        elif key == 105:
            self.replace_tile("I",self.mouse_pos[0],self.mouse_pos[1])
        elif key in strg:
            self.replace_tile(str(key-48),self.mouse_pos[0],self.mouse_pos[1])
        elif key == 103 and self.guide and self.command():
            self.guide = False
            handle = open('Guide.txt')
            for line in handle:
                print(line[:-1])


        
    def trim(self):
        strg = self.world.copy()
        strgc = False
        if strg == [' '*len(strg[0])]*len(strg):
            return None
        strgb = True
        while strgb:
            strgb = True
            for j in range(0,len(strg)):
                if strg[j][-WIDTH:] == ' '*WIDTH:
                    continue
                strgb = False
            if strgb:
                if WIDTH*self.chunkcoords[0] == len(strg[0]):
                    strgc = True
                    self.chunkcoords[0] = self.chunkcoords[0] - 1
                for j in range(0,len(strg)):
                    strg[j] = strg[j][:-WIDTH]
        strgb = True
        while strgb:
            strgb = True
            for j in range(len(strg)-HEIGHT, len(strg)):
                if strg[j] == ' '*len(strg[0]):
                    continue
                strgb = False
            if strgb:
                if HEIGHT*self.chunkcoords[1] == len(strg):
                    strgc = True
                    self.chunkcoords[1] = self.chunkcoords[1] - 1
                strg = strg[:-HEIGHT]
        if strgc:
            self.loadchunk(self.chunkcoords[0],self.chunkcoords[1])
        self.world = strg.copy()
    
    def tip_display(self, theStr):
        if self.tipTimer > -1:
            self.tipTimer = 0
        x, y = self.tip.pos()
        self.tip.destroy()
        self.tip = games.Text(self, x, y, theStr, int(TEXT_SIZE*1.3), color.RED)
        if self.tipTimer == -1:
            self.tipTimer = 0

    def save_world(self):
        if len(self.world) < 2*HEIGHT:
            self.world.append('W'*len(self.world[0]))
            for i in range(1, HEIGHT):
                self.world.append(' '*len(self.world[0]))
        if len(self.world[0]) < 2*WIDTH:
            for j in range(0, len(self.world)):
                self.world[j] += 'W'+(' '*(WIDTH-1))
        handle = open('WORLD.txt', 'r+')
        handle.truncate()
        for line in range(0, len(self.world)-1):
            handle.write(self.world[line]+'\n')
        handle.write(self.world[-1])
        handle.close()

    def mouse_down(self, xy, button):
        self.selecting = True
        x, y = self.coords_to_cell(xy[0], xy[1])
        if self.on_board(x, y):
            self.old_click = (x, y)
            if self.grid[x][y].selected:
                self.selecting = False
                self.dragging = True
                for i in range(self.selected_coords[0][0],self.selected_coords[1][0]+1):
                    for j in range(self.selected_coords[0][1],self.selected_coords[1][1]+1):
                        if self.command():
                            self.grid[i][j].deselect()
                        else:
                            self.replace_tile(' ', i, j)

    def mouse_up(self, xy, button):
        if self.dragging:
            I = None
            J = None
            for i in range(self.selected_coords[0][0]-self.old_click[0]+self.mouse_pos[0], self.selected_coords[1][0]-self.old_click[0]+self.mouse_pos[0]+1):
                for j in range(self.selected_coords[0][1]-self.old_click[1]+self.mouse_pos[1], self.selected_coords[1][1]-self.old_click[1]+self.mouse_pos[1]+1):
                    if J == None:
                        J = j
                    if I == None:
                        I = i
                    self.replace_tile(self.selection[j-J][i-I], i, j)
                    if self.on_board(i, j):
                        self.grid[i][j].tileColor = self.selected_colors[j-J][i-I]
                        self.grid[i][j].set_color(self.grid[i][j].tileColor)
        if self.selecting:
            self.selected_coords[0][0] = min(self.old_click[0],self.mouse_pos[0])
            self.selected_coords[0][1] = min(self.old_click[1],self.mouse_pos[1])
            self.selected_coords[1][0] = max(self.old_click[0],self.mouse_pos[0])
            self.selected_coords[1][1] = max(self.old_click[1],self.mouse_pos[1])
            self.sel_width = self.selected_coords[1][0]-self.selected_coords[0][0]+1
            self.sel_height = self.selected_coords[1][1]-self.selected_coords[0][1]+1
            self.selection = [' '*(self.selected_coords[1][0]-self.selected_coords[0][0]+1)]*(self.selected_coords[1][1]-self.selected_coords[0][1]+1)
            self.selected_colors = [['']*(self.selected_coords[1][0]-self.selected_coords[0][0]+1)]*(self.selected_coords[1][1]-self.selected_coords[0][1]+1)
            I = -1
            J = -1
            for i in range(self.selected_coords[0][0],self.selected_coords[1][0]+1):
                I += 1
                for j in range(self.selected_coords[0][1],self.selected_coords[1][1]+1):
                    J += 1
                    self.selection[J] = self.selection[J][:I] + self.chunk[j][i] + self.selection[J][(I+1):]
                    self.selected_colors[J] = self.selected_colors[J][:I] + [self.grid[i][j].tileColor] + self.selected_colors[J][(I+1):]
                J = -1
        self.selecting = False
        self.dragging = False

    def command(self):
        return (self.is_pressed(COMMAND[0]) or self.is_pressed(COMMAND[1]))

    def reset_world(self):
        self.world = [' '*WIDTH]*HEIGHT
        self.loadchunk(1,1)

    def makeWorld(self):
        step = WIDTH
        handle = open('WORLD.txt')
        for line in handle:
            self.world.append(line[:-1])
        while not len(self.world)//step == len(self.world)/step:
            self.world.append('')
        geowidth = 0
        for line in self.world:
            if len(line)>geowidth:
                geowidth = len(line)
        if not geowidth//step == geowidth/step:
            geowidth = step*(geowidth//step+1)
        j = -1
        for line in self.world:
            j+=1
            while not len(self.world[j]) == geowidth:
                self.world[j] += ' '
        for line in self.world[:HEIGHT]:
            self.chunk.append(line[:WIDTH])

    def tick(self):
        if self.tipTimer > -1:
            self.tipTimer += 1
        if self.tipTimer == 500:
            self.tip_display('')
        if self.on_board(self.coords_to_cell(self.mouse_position()[0],self.mouse_position()[1])[0],self.coords_to_cell(self.mouse_position()[0],self.mouse_position()[1])[1]):
            self.mouse_pos = self.coords_to_cell(self.mouse_position()[0],self.mouse_position()[1])
        if self.selecting:
            for i in range(0, len(self.grid)):
                for j in range(0, len(self.grid[0])):
                    if i <= max(self.old_click[0],self.mouse_pos[0]) and i >= min(self.old_click[0],self.mouse_pos[0]) and j <= max(self.old_click[1],self.mouse_pos[1]) and j >= min(self.old_click[1],self.mouse_pos[1]):
                        self.grid[i][j].select()
                    else:
                        self.grid[i][j].deselect()
        elif self.dragging:
            I = None
            J = None
            while (self.chunkcoords[0] == 1 and (self.selected_coords[0][0]-self.old_click[0]+self.mouse_pos[0] < 0)):
                self.old_click = (self.old_click[0]-1,self.old_click[1])
            while (self.chunkcoords[1] == 1 and (self.selected_coords[0][1]-self.old_click[1]+self.mouse_pos[1] < 0)):
                self.old_click = (self.old_click[0],self.old_click[1]-1)
            for i in range(self.selected_coords[0][0]-self.old_click[0]+self.mouse_pos[0], self.selected_coords[1][0]-self.old_click[0]+self.mouse_pos[0]+1):
                for j in range(self.selected_coords[0][1]-self.old_click[1]+self.mouse_pos[1], self.selected_coords[1][1]-self.old_click[1]+self.mouse_pos[1]+1):
                    if J == None:
                        J = j
                    if I == None:
                        I = i
                    if self.on_board(i, j):
                        self.grid[i][j].set_color(self.selected_colors[j-J][i-I])
            for i in range(0,len(self.grid)):
                for j in range(0,len(self.grid[0])):
                    if i <= self.selected_coords[1][0]-self.old_click[0]+self.mouse_pos[0] and i >= self.selected_coords[0][0]-self.old_click[0]+self.mouse_pos[0] and j <= self.selected_coords[1][1]-self.old_click[1]+self.mouse_pos[1] and j >= self.selected_coords[0][1]-self.old_click[1]+self.mouse_pos[1]:
                        pass
                    else:
                        self.grid[i][j].set_color(self.grid[i][j].tileColor) 
        if self.is_pressed(103) and not self.command():
            self.replace_tile("#",self.mouse_pos[0],self.mouse_pos[1])
        elif self.is_pressed(109):
            self.replace_tile("M",self.mouse_pos[0],self.mouse_pos[1])
        elif self.is_pressed(112):
            self.replace_tile("P",self.mouse_pos[0],self.mouse_pos[1])
        elif self.is_pressed(115) and not self.command():
            self.replace_tile("S",self.mouse_pos[0],self.mouse_pos[1])
        elif self.is_pressed(119):
            self.replace_tile("W",self.mouse_pos[0],self.mouse_pos[1])
        elif self.is_pressed(32):
            self.replace_tile(" ",self.mouse_pos[0],self.mouse_pos[1])

    def loadchunk(self, chunkX, chunkY):
        if chunkX < 1 or chunkY < 1:
            return None
        if chunkX > len(self.world[0])/WIDTH:
            for j in range(0,len(self.world)):
                self.world[j] += ' '*WIDTH
        if chunkY > len(self.world)/HEIGHT:
            for j in range(0,HEIGHT):
                self.world.append(' '*len(self.world[0]))
        self.chunkcoords[0] = chunkX
        self.chunkcoords[1] = chunkY
        for i in self.grid:
            for j in i:
                j.destroy()
        i, j = -1, -1
        self.chunk = []
        for line in self.world[(HEIGHT*(chunkY-1)):(HEIGHT*chunkY)]:
            j += 1
            i = -1
            self.chunk.append(self.world[j+HEIGHT*(chunkY-1)][(WIDTH*(chunkX-1)):(WIDTH*chunkX)])
            for tile in self.world[(WIDTH*(chunkX-1)):(WIDTH*chunkX)]:
                i += 1
        self.init_gameboard(self, (MARGIN,MARGIN), WIDTH, HEIGHT, BOX_SIZE)
        if self.chunkcoords == [1,1]:
            x, y = self.spawn_indicator.pos()
            self.spawn_indicator.destroy()
            self.spawn_indicator = games.Text(self, x, y, "S", round(1.5*BOX_SIZE), color.RED)
        else:
            x, y = self.spawn_indicator.pos()
            self.spawn_indicator.destroy()
            self.spawn_indicator = games.Text(self, x, y, "", round(1.5*BOX_SIZE), color.RED)


    def fill(self, fillTile):
        for i in range(0, WIDTH):
            for j in range(0, HEIGHT):
                if self.grid[i][j].selected:
                    self.grid[i][j].deselect()
                    self.replace_tile(fillTile, i, j)

    def replace_tile(self, tile, i, j):
        try:
            if self.grid[i][j].selected:
                self.fill(tile)
                return None
        except:
            pass
        if tile in '123456789':
            count = 0
            for I in self.world:
                for J in I:
                    if J == tile:
                        count += 1
            if count >= 2:
                self.tip_display(f'You Already Have 2 Teleporters With That ID')
                return None
        if tile == 'R':
            count = 0
            for I in self.world:
                for J in I:
                    if J == tile:
                        count += 1
            if count > 0:
                self.tip_display('You Can Only Have 1 Raft Per World')
                return None
        if tile == 'B':
            count = 0
            for I in self.world:
                for J in I:
                    if J == tile:
                        count += 1
            if count >= 3:
                self.tip_display('You Cannot Have More Than 3 Bosses In 1 World')
                return None
        x = i + WIDTH*self.chunkcoords[0]-WIDTH
        y = j + HEIGHT*self.chunkcoords[1]-HEIGHT
        while x >= len(self.world[0]):
            for I in range(0,len(self.world)):
                self.world[I] += ' '*WIDTH
        while y >= len(self.world):
            for I in range(0, HEIGHT):
                self.world.append(' '*len(self.world[0]))
        if self.world[y][x] == tile:
            return None
        self.world[y] = self.world[y][:x] + tile + self.world[y][(x+1):]
        if not self.on_board(i, j):
            return None
        self.chunk[j] = self.chunk[j][:i] + tile + self.chunk[j][(i+1):]
        try:
            self.grid[i][j].label.destroy()
        except:
            pass
        self.grid[i][j].destroy()
        self.grid[i][j] = self.new_gamecell(i, j)
        if not (i,j) == (WIDTH//2, HEIGHT//2):
            return None
        if self.chunkcoords == [1,1]:
            x, y = self.spawn_indicator.pos()
            self.spawn_indicator.destroy()
            self.spawn_indicator = games.Text(self, x, y, "S", round(1.5*BOX_SIZE), color.RED)
        else:
            x, y = self.spawn_indicator.pos()
            self.spawn_indicator.destroy()
            self.spawn_indicator = games.Text(self, x, y, "", round(1.5*BOX_SIZE), color.RED)


class Square(boards.GameCell):
    def __init__(self, board, i, j):
        self.tileColor = color.BLACK
        self.init_tile(board, i, j)

    def init_tile(self, board, i, j):
        self.selected = False
        self.board = board
        self.init_gamecell(board, i, j)

    def select(self):
        strg = []
        if str(type(self.tileColor)) == "<class 'str'>":
            self.tileColor = hex_to_rgb(self.tileColor)
        for i in range(0,3):
            strg.append((self.tileColor[i]+255)//2)
        self.selected = True
        self.set_color(tuple(strg))

    def deselect(self):
        self.selected = False
        self.set_color(self.tileColor)


class Sand(Square):
    def __init__(self, board, i, j):
        self.init_tile(board, i, j)
        self.tileColor = [(219,204,125),(179,165,96)][randint(0,1)]
        self.set_color(self.tileColor)

class Mud(Square):
    def __init__(self, board, i, j):
        self.init_tile(board, i, j)
        self.tileColor = [(54,41,19),(70,60,31)][randint(0,1)]
        self.set_color(self.tileColor)

class Grass(Square):
    def __init__(self, board, i, j):
        self.init_tile(board, i, j)
        self.tileColor = [(71,194,14),(86,237,17)][randint(0,1)]
        self.set_color(self.tileColor)

class Plank(Square):
    def __init__(self, board, i, j):
        self.init_tile(board, i, j)
        self.tileColor = [(184, 145, 79),(153,120,63)][randint(0,1)]
        self.set_color(self.tileColor)

class Wall(Square):
    def __init__(self, board, i, j):
        self.init_tile(board, i, j)
        self.tileColor = [color.DARKGRAY,color.GRAY][randint(0,1)]
        self.set_color(self.tileColor)

class Checkpoint(Square):
    def __init__(self, board, i, j):
        self.init_tile(board, i, j)
        self.tileColor = (27, 0, 125)
        self.set_color(self.tileColor)

class Teleporter(Square):
    def __init__(self, board, i, j):
        self.init_tile(board, i, j)
        self.tileColor = color.PURPLE
        self.set_color(self.tileColor)
        self.tpID = self.board.world[j+HEIGHT*(self.board.chunkcoords[1]-1)][i+WIDTH*(self.board.chunkcoords[0]-1)]
        x, y = self.board.cell_to_coords(i+0.5, j+0.5)
        self.label = games.Text(self.board, x, y, str(self.tpID), round(1.5*BOX_SIZE), color.RED)

class Raft(Square):
    def __init__(self, board, i, j):
        self.init_tile(board, i, j)
        self.tileColor = color.ORANGE
        self.set_color(self.tileColor)

class WorldItem(Square):
    def __init__(self, board, i, j):
        self.init_tile(board, i, j)
        self.tileColor = color.GOLD
        self.set_color(self.tileColor)

class Boss(Square):
    def __init__(self, board, i, j):
        self.init_tile(board, i, j)
        self.tileColor = (200, 0, 0)
        self.set_color(self.tileColor)

class Water(Square):
    def __init__(self, board, i, j):
        self.init_tile(board, i, j)
        self.tileColor = color.LIGHTBLUE
        self.set_color(self.tileColor)

game = WorldMaker()
game.mainloop()
