from gasp import games
from gasp import color
from gasp import boards
from classes.game import Person
from classes.magic import Spell
from classes.inventory import Item
import vlc
from random import randint

BOX_SIZE = 20
MARGIN = 30
WIDTH = 30
HEIGHT = WIDTH
TEXT_SIZE = 20
ENCOUNTER = 20
SAVE_FILE = "SaveData.txt"
LETTERCOLORS = {'#': color.BLACK,
        ' ': (219,219,219),
        'B': color.BLUE,
        'G': color.GREEN,
        'R': color.RED,
        'Y': color.YELLOW,
        'P': color.PURPLE,
        'O': color.ORANGE,
        'W': color.WHITE}
OVERWORLD_MUSIC = "Zelda.mp3"
BATTLE_MUSIC = "Pokemon.mp3"
BOSS_MUSIC = "FinalFantasyVIIBoss.mp3"
MEME = "RickRoll8Bit.mp3"
MEGALOVANIA = "Megalovania8Bit.mp3"
RICK_ASTLEY = 5
SANS = 13
NUM_LAND_ENEMIES = 6
NUM_WATER_ENEMIES = 2
NUM_CAVE_ENEMIES = 2
NUM_NORMAL_ENEMIES = NUM_LAND_ENEMIES + NUM_WATER_ENEMIES + NUM_CAVE_ENEMIES
NUM_BOSSES = 3
BOSS_LEVELS = [30,70]
MENU_OPTIONS = {
        1: ['Fight','Bag','Magic','Run'],
        2: ['Fire', 'Cure', 'Absorb', 'Back'],
        3: ['HP', 'MP', 'Damage', 'Back'],
        4: ['Apple', 'Potion', 'Steak', 'Back'],
        5: ['Cupcake', 'Starbit', 'Dragon Breath', 'Back'],
        6: ['Rock', 'Shuriken', 'Arrow', 'Back']
        }
BAG_MENU_OPTIONS = {
        1: ['HP','MP','Cure','Back'],
        2: ['Apple', 'Potion', 'Steak', 'Back'],
        3: ['Cupcake', 'Starbit', 'Dragon Breath', 'Back']
        }
DESCRIPTIONS = [
        [
            'Does Damage To The Enemy',
            'Allows You To Use Items To Heal, Restore MP, Do Damage, Etc',
            'Allows You To Spend MP For A Variety Of Different Effects',
            'Allows You To Flee The Battle, Will Occasionally Fail'],
        [
            'Cost: 10MP | Sets Opponents Ablaze To Deal Big Damage',
            'Cost: 8MP | Heals All Damage Taken',
            'Cost: 12MP | Does Damage And Steals Some Of The Enemy\'s HP',
            'Return To Previous Menu'],
        [
            'Healing Items',
            'MP Restoring Items',
            'Damage Dealing Items',
            'Return To Previous Menu'],
        [
            'Heals 70 HP',
            'Heals 100 HP',
            'Heals All Damage Taken',
            'Return To Previous Menu'],
        [
            'Restores 20MP',
            'Restores 50MP',
            'Restores All MP',
            'Return To Previous Menu'],
        [
            'Deals 35 Damage',
            'Deals 70 Damage',
            'Deals 120 Damage',
            'Return To Previous Menu']
        ]
BAG_DESCRIPTIONS = [
    [
        DESCRIPTIONS[2][0],
        DESCRIPTIONS[2][1],
        DESCRIPTIONS[1][1],
        'Close Bag'],
    [
        DESCRIPTIONS[3][0],
        DESCRIPTIONS[3][1],
        DESCRIPTIONS[3][2],
        DESCRIPTIONS[3][3]],
    [
        DESCRIPTIONS[4][0],
        DESCRIPTIONS[4][1],
        DESCRIPTIONS[4][2],
        DESCRIPTIONS[4][3]]
    ]



class RPGgame(boards.SingleBoard):
    def __init__(self):
        self.loadFile = ''
        while not self.loadFile in ['YES', 'NO']:
            self.loadFile = input('Load Your Save File? (Type YES or NO) ')
            self.loadFile = self.loadFile.upper()
        self.loadFile = {'YES': True, 'NO': False}[self.loadFile]
        self.myMessage = ''
        self.battle = False
        self.bag_menu = False
        self.world = []
        self.chunk = []
        self.spawnpoint = [WIDTH//2, HEIGHT//2]
        self.walkableTiles=['#','P','B','I','M','S','R','C']
        for w in range(1,10):
            self.walkableTiles.append(str(w))
        self.battleMusic = vlc.MediaPlayer(BATTLE_MUSIC)
        self.bossMusic = vlc.MediaPlayer(BOSS_MUSIC)
        self.meme = vlc.MediaPlayer(MEME)
        self.megalovania = vlc.MediaPlayer(MEGALOVANIA)
        self.overworldMusic = vlc.MediaPlayer(OVERWORLD_MUSIC)
        self.makeWorld()
        self.chunkcoords = [1,1]
        self.init_singleboard((MARGIN, MARGIN), WIDTH, HEIGHT, BOX_SIZE)
        self.metronome = 0
        self.draw_all_outlines()
        self.player = Player(self)
        self.loadchunk(self.chunkcoords[0],self.chunkcoords[1])
        self.move_player(self.player.x,self.player.y)
        self.mousePos = (0,0)
        self.oldMousePos = (0,0)
        self.randEncounter = False
        self.overworldMusic.play()
        self.bossesBeaten = 0
        #battle options
        self.fire = Spell("Fire", 10, 60, "black")
        self.cure = Spell("Cure", 8, 9999, "white")
        self.absorb = Spell("Absorb", 12, 40, "white")
        self.apple = Item("Apple", "potion", "Heals 70 HP", 70)
        self.potion = Item("Potion", "potion", "Heals 100 HP", 100)
        self.steak = Item("Steak", "potion", "Heals All Damage", 9999)
        self.cupcake = Item("Cupcake", "elixer", "Restores 20 MP", 20)
        self.starbit = Item("Starbit", "elixer", "Restores 50 MP", 50)
        self.dragonbreath = Item("Dragon Breath", "elixer", "Restores All MP", 9999)
        self.rock = Item("Rock", "weapon", "Deals 35 Damage", 35)
        self.shuriken = Item("Shuriken", "weapon", "Deals 70 Damage", 70)
        self.arrow = Item("Arrow", "weapon", "Deals 120 Damage", 120)

        self.myMagic = [self.fire, self.cure, self.absorb]
        self.myItems = [
                {"item": self.apple, "quantity": 20},
                {"item": self.potion, "quantity": 10},
                {"item": self.steak, "quantity": 2},
                {"item": self.cupcake, "quantity": 20},
                {"item": self.starbit, "quantity": 10},
                {"item": self.dragonbreath, "quantity": 2},
                {"item": self.rock, "quantity": 20},
                {"item": self.shuriken, "quantity": 10},
                {"item": self.arrow, "quantity": 2}
                ]
        self.lvl = 5
        self.exp = 0
        self.max_exp = 10
        self.avatar = Person(100, 40, 30, 20, self.myMagic, self.myItems)
        self.layer = 1
        self.blayer = 1
        self.enemyType=1
        self.elvl = 1
        self.magic_power = 0
        x, y = self.cell_to_coords(WIDTH/2,HEIGHT-1)
        self.description = games.Text(self, x, y, '', TEXT_SIZE, color.WHITE)
        x, y = self.cell_to_coords(WIDTH/2, 5*(HEIGHT//6)-2)
        self.comment = games.Text(self, x, y, '', TEXT_SIZE, color.WHITE)
        x, y = self.cell_to_coords(WIDTH/2,5.5)
        self.bag_description = games.Text(self, x, y, '', TEXT_SIZE, color.WHITE)
        x, y = self.cell_to_coords(WIDTH/2,HEIGHT-1.5)
        self.tip = games.Text(self, x, y, '', TEXT_SIZE, color.WHITE)
        self.tipTimer = -1
        if self.loadFile:
            self.load_game()

    def save_game(self):
        handle = open(SAVE_FILE)
        lines = []
        i = -1
        for line in handle:
            i += 1
            if i > 23:
                break
            lines.append(line.split(':')[0]+': ')
        handle.close()
        lines[0] += str(self.player.geoX)
        lines[1] += str(self.player.geoY)
        lines[2] += str(self.avatar.atkl)
        lines[3] += str(self.avatar.atkh)
        lines[4] += str(self.magic_power)
        lines[5] += str(self.avatar.df)
        lines[6] += str(self.avatar.hp)
        lines[7] += str(self.avatar.max_hp)
        lines[8] += str(self.avatar.mp)
        lines[9] += str(self.avatar.max_mp)
        lines[10] += str(self.exp)
        lines[11] += str(self.max_exp)
        lines[12] += str(self.lvl)
        lines[13] += str(self.bossesBeaten)
        lines[14] += str(' ' in self.walkableTiles)
        lines[15] += str(self.myItems[0]["quantity"])
        lines[16] += str(self.myItems[1]["quantity"])
        lines[17] += str(self.myItems[2]["quantity"])
        lines[18] += str(self.myItems[3]["quantity"])
        lines[19] += str(self.myItems[4]["quantity"])
        lines[20] += str(self.myItems[5]["quantity"])
        lines[21] += str(self.myItems[6]["quantity"])
        lines[22] += str(self.myItems[7]["quantity"])
        lines[23] += str(self.myItems[8]["quantity"])
        for line in range(0,len(lines)):
            lines[line] += '\n'
        lines.append('world: \n')
        for line in self.world:
            lines.append(line+'\n')
        lines[-1] = lines[-1][:-1]
        handle = open(SAVE_FILE, 'r+')
        handle.truncate()
        for line in lines:
            handle.write(line)
        handle.close()
        
    def load_game(self):
        handle = open(SAVE_FILE)
        strg = []
        lines = []
        i = -1
        for line in handle:
            i += 1
            if i > 24:
                strg.append(line[:-1])
            elif i < 24:
                lines.append(line.split(': ')[1][:-1])
        handle.close()
        self.world = strg.copy()
        self.player.geoX = int(lines[0])
        self.player.geoY = int(lines[1])
        self.spawnpoint[0] = int(lines[0])
        self.spawnpoint[1] = int(lines[1])
        self.avatar.atkl = int(lines[2])
        self.avatar.atkh = int(lines[3])
        self.magic_power = int(lines[4])
        self.avatar.df = int(lines[5])
        self.avatar.hp = int(lines[6])
        self.avatar.max_hp = int(lines[7])
        self.avatar.mp = int(lines[8])
        self.avatar.max_mp = int(lines[9])
        self.exp = int(lines[10])
        self.max_exp = int(lines[11])
        self.lvl = int(lines[12])
        self.bossesBeaten = int(lines[13])
        if lines[14] == 'True':
            self.walkableTiles.append(' ')
        for i in range(15,24):
            self.myItems[i-15]["quantity"] = int(lines[i])
        self.player.teleport(self.player.geoX,self.player.geoY)

    def makeWorld(self):
        step = WIDTH
        handle = open('WORLD.txt')
        for line in handle:
            self.world.append(line[:-1])
        while not len(self.world)//step == len(self.world)/step:
            self.world.append('')
        geowidth = 0
        for line in self.world:
            if len(line)>geowidth:
                geowidth = len(line)
        if not geowidth//step == geowidth/step:
            geowidth = step*(geowidth//step+1)
        j = -1
        for line in self.world:
            j+=1
            while not len(self.world[j]) == geowidth:
                self.world[j] += ' '
        for line in self.world[:HEIGHT+1]:
            self.chunk.append(line[:WIDTH+1])

    def load_layer_options(self, layer):
        self.oldMousePos = None
        self.layer = layer
        for i in self.grid:
            for j in i:
                try:
                    x, y = self.cell_to_coords(j.x, j.y)
                    extra = ''
                    if layer >= 4 and layer <= 6 and j.ID < 4:
                        extra = ' x '+str(self.myItems[3*layer+j.ID-13]["quantity"])
                    j.replace_text(MENU_OPTIONS[self.layer][j.ID-1]+extra, x, y)
                except:
                    pass

    def load_blayer_options(self, layer):
        self.oldMousePos = None
        self.blayer = layer
        for i in self.grid:
            for j in i:
                try:
                    x, y = self.cell_to_coords(j.x, j.y)
                    extra = ''
                    if layer > 1 and j.ID < 4:
                        extra = ' x '+str(self.myItems[3*layer+j.ID-7]["quantity"])
                    j.replace_text(BAG_MENU_OPTIONS[self.blayer][j.ID-1]+extra, x, y)
                except:
                    pass

    def replace_text(self):
        x, y = self.myHPText.pos()
        self.myHPText.destroy()
        self.myHPText = games.Text(self, x, y, f'HP: {self.avatar.get_hp()}/{self.avatar.get_max_hp()}', int(BOX_SIZE*1.5), color.GREEN)
        x, y = self.myMPText.pos()
        self.myMPText.destroy()
        self.myMPText = games.Text(self, x, y, f'MP: {self.avatar.get_mp()}/{self.avatar.get_max_mp()}', int(BOX_SIZE*1.5), color.BLUE)
        x, y = self.enemyHPText.pos()
        self.enemyHPText.destroy()
        self.enemyHPText = games.Text(self, x, y, f'HP: {self.enemy.get_hp()}/{self.enemy.get_max_hp()}', int(BOX_SIZE*1.5), color.RED)

    def replace_bag_text(self):
        x, y = self.myBagHPText.pos()
        self.myBagHPText.destroy()
        self.myBagHPText = games.Text(self, x, y, f'HP: {self.avatar.get_hp()}/{self.avatar.get_max_hp()}', int(BOX_SIZE*1.5), color.GREEN)
        x, y = self.myBagMPText.pos()
        self.myBagMPText.destroy()
        self.myBagMPText = games.Text(self, x, y, f'MP: {self.avatar.get_mp()}/{self.avatar.get_max_mp()}', int(BOX_SIZE*1.5), color.BLUE)

    def new_gamecell(self, i, j):
        strg = []
        for w in range(1,10):
            strg.append(str(w))
        if self.bag_menu:
            ids = {(5*WIDTH//6, 1): 2, (WIDTH//2, 1): 1, (5*WIDTH//6, 3): 4, (WIDTH//2, 3): 3}
            if (i,j) in ids.keys():
                return OButton(self, i, j, ids[(i,j)])
            elif j == 7:
                return OBar(self, i, j)
            else:
                return OMenu(self, i, j)
        elif not self.battle:
            if self.chunk[j][i] == "#":
                return Grass(self, i, j)
            elif self.chunk[j][i] == " ":
                return Water(self, i, j)
            elif self.chunk[j][i] == "S":
                return Sand(self, i, j)
            elif self.chunk[j][i] == "W":
                return Wall(self, i, j)
            elif self.chunk[j][i] == "M":
                return Mud(self, i, j)
            elif self.chunk[j][i] == "P":
                return Plank(self, i, j)
            elif self.chunk[j][i] == "C":
                return Checkpoint(self, i, j)
            elif self.chunk[j][i] == "R":
                return Raft(self, i, j)
            elif self.chunk[j][i] == "I":
                return WorldItem(self, i, j)
            elif self.chunk[j][i] == "B":
                return Boss(self, i, j)
            elif self.chunk[j][i] in strg:
                return Teleporter(self, i, j)
        else:
            if j in [5*(HEIGHT//6)-1,5*(HEIGHT//6)+1] and i in [WIDTH//4,2*WIDTH//3]:
                return BButton(self, i, j)
            elif j == 2*(HEIGHT//3):
                return BBar(self, i, j)
            elif j > 2*(HEIGHT//3):
                return BMenu(self, i, j)
            elif j >= HEIGHT//2 and i > WIDTH//10 and i < WIDTH//3:
                return BPlayer(self, i, j)
            elif i < WIDTH-2 and j > 1 and i > WIDTH-11 and j < 10:
                return BEnemy(self, i, j)
            else:
                return BBackground(self, i, j)

    def loadchunk(self, chunkX, chunkY):
        for i in self.grid:
            for j in i:
                if not j == self.player:
                    j.destroy()
        i, j = -1, -1
        self.chunk = []
        for line in self.world[(HEIGHT*(chunkY-1)):(HEIGHT*chunkY)]:
            j += 1
            i = -1
            self.chunk.append(self.world[j+HEIGHT*(chunkY-1)][(WIDTH*(chunkX-1)):(WIDTH*chunkX)])
            for tile in self.world[(WIDTH*(chunkX-1)):(WIDTH*chunkX)]:
                i += 1
        self.init_gameboard(self, (MARGIN,MARGIN), WIDTH, HEIGHT, BOX_SIZE)

    def encounter(self):
        self.tipTimer = 499
        self.overworldMusic.stop()
        self.enemyType = randint(1,NUM_LAND_ENEMIES)
        if self.world[self.player.geoY][self.player.geoX] in [' ', 'S']:
            self.enemyType = randint(NUM_LAND_ENEMIES+1, NUM_LAND_ENEMIES+NUM_WATER_ENEMIES)
        if self.world[self.player.geoY][self.player.geoX] == 'M':
            self.enemyType = randint(NUM_LAND_ENEMIES+NUM_WATER_ENEMIES+1, NUM_LAND_ENEMIES+NUM_WATER_ENEMIES+NUM_CAVE_ENEMIES)
        if self.world[self.player.geoY][self.player.geoX] == 'B':
            self.enemyType = NUM_LAND_ENEMIES+NUM_WATER_ENEMIES+NUM_CAVE_ENEMIES+self.bossesBeaten+1
        self.elvl = randint(self.lvl-7,self.lvl+1)
        if self.enemyType > NUM_LAND_ENEMIES+NUM_WATER_ENEMIES+NUM_CAVE_ENEMIES:
            self.elvl = int(BOSS_LEVELS[0]+(self.bossesBeaten*((BOSS_LEVELS[1]-BOSS_LEVELS[0])/(NUM_BOSSES-1))))
        if self.elvl < 1:
            self.elvl = 1
        if self.elvl > 100:
            self.elvl = 100
        self.randEncounter = True
        x, y = self.cell_to_coords(self.player.x+0.5, self.player.y-0.5)
        self.battleMark = games.Text(self, x, y, "!", round(1.5*BOX_SIZE), color.RED)
        if self.enemyType == RICK_ASTLEY:
            self.meme.play()
        elif self.enemyType == SANS:
            self.megalovania.play()
        elif self.enemyType > NUM_NORMAL_ENEMIES:
            self.bossMusic.play()
        elif str(self.battleMusic.get_state()) != 'State.Playing':
            self.battleMusic.play()
        self.layer = 1


    def move_player(self, newX, newY):
        for i in self.grid:
            for j in i:
                j.set_color(j.tileColor)
        self.grid[newX][newY].set_color(color.RED)

    def tip_display(self, theStr):
        if self.tipTimer > -1:
            self.tipTimer = 0
        x, y = self.tip.pos()
        self.tip.destroy()
        self.tip = games.Text(self, x, y, theStr, int(TEXT_SIZE*1.3), color.BLUE)
        if self.tipTimer == -1:
            self.tipTimer = 0

    def load_bag(self):
        self.bag_menu = True
        self.blayer = 1
        for i in range(0,len(self.grid)):
            for j in range(0,len(self.grid[i])):
                if j < 8:
                    self.grid[i][j].destroy()
                    self.grid[i][j] = self.new_gamecell(i,j)
        x, y = self.cell_to_coords(WIDTH//6,1.5)
        self.myBagHPText = games.Text(self, x, y, f'HP: {self.avatar.get_hp()}/{self.avatar.get_max_hp()}', int(BOX_SIZE*1.5), color.GREEN)
        x, y = self.cell_to_coords(WIDTH//6,2.5)
        self.myBagMPText = games.Text(self, x, y, f'MP: {self.avatar.get_mp()}/{self.avatar.get_max_mp()}', int(BOX_SIZE*1.5), color.BLUE)
        x, y = self.cell_to_coords(WIDTH//6,3.5)
        self.myBagLVLText = games.Text(self, x, y, f'Lvl. {self.lvl}', int(BOX_SIZE*1.5), color.PURPLE)
        x, y = self.cell_to_coords(WIDTH//6,4.5)
        self.myBagEXPText = games.Text(self, x, y, f'EXP: {self.exp}/{self.max_exp}', int(BOX_SIZE*1.5), color.YELLOW)

    def close_bag(self):
        self.bag_menu = False
        self.blayer = 1
        for i in self.grid:
            for j in i:
                try:
                    j.label.destroy()
                except:
                    pass
        for i in range(0,len(self.grid)):
            for j in range(0,len(self.grid[i])):
                if j < 8:
                    self.grid[i][j].destroy()
                    self.grid[i][j] = self.new_gamecell(i,j)
        self.move_player(self.player.x, self.player.y)
        self.myBagHPText.destroy()
        self.myBagMPText.destroy()
        self.myBagLVLText.destroy()
        self.myBagEXPText.destroy()

    def tick(self):
        if self.tipTimer > -1:
            self.tipTimer += 1
        if self.tipTimer == 500:
            self.tip_display('')
        if str(self.battleMusic.get_state()) == 'State.Ended':
            self.battleMusic.stop()
            self.battleMusic.play()
        if str(self.meme.get_state()) == 'State.Ended':
            self.meme.stop()
            self.meme.play()
        if str(self.megalovania.get_state()) == 'State.Ended':
            self.megalovania.stop()
            self.megalovania.play()
        if str(self.overworldMusic.get_state()) == 'State.Ended':
            self.overworldMusic.stop()
            self.overworldMusic.play()
        if str(self.bossMusic.get_state()) == 'State.Ended':
            self.bossMusic.stop()
            self.bossMusic.play()
        self.mousePos = self.coords_to_cell(self.mouse_position()[0],self.mouse_position()[1])
        if self.on_board(self.mousePos[0],self.mousePos[1]) and self.mousePos != self.oldMousePos:
            self.grid[self.mousePos[0]][self.mousePos[1]].hoverAction()
        self.oldMousePos = self.mousePos
        if self.randEncounter:
            self.metronome += 1
            if self.metronome == 100:
                for i in self.grid:
                    for j in i:
                        j.set_color(color.BLACK)
                self.battleMark.set_text('')
            if self.metronome == 200:
                self.metronome = 0
                self.randEncounter = False
                self.battle = True
                for I in self.grid:
                    for J in I:
                        if not J == self.player:
                            J.destroy()
                self.grid = []
                handle = open(f'enemy{self.enemyType}.txt')
                self.enemyArt = []
                for line in handle:
                    self.enemyArt.append(line[:-1])
                self.init_gameboard(self, (MARGIN,MARGIN), WIDTH, HEIGHT, BOX_SIZE)
                hp = 0
                atk = 0
                if self.enemyType > NUM_LAND_ENEMIES+NUM_WATER_ENEMIES+NUM_CAVE_ENEMIES:
                    hp = 10*self.elvl
                    atk = 3*self.elvl
                    if self.enemyType == NUM_NORMAL_ENEMIES + NUM_BOSSES:
                        atk += 2*self.elvl
                self.enemy = Person(60+hp+randint(3*self.elvl,9*self.elvl), 35+randint(self.elvl,3*self.elvl), 35+atk+randint(self.elvl,3*self.elvl), 35+randint(self.elvl,5*self.elvl), [], [])
                x, y = self.cell_to_coords(WIDTH//6+2,HEIGHT//2-2.5)
                self.myHPText = games.Text(self, x, y, f'HP: {self.avatar.get_hp()}/{self.avatar.get_max_hp()}', int(BOX_SIZE*1.5), color.GREEN)
                x, y = self.cell_to_coords(WIDTH//6+2,HEIGHT//2-1.5)
                self.myMPText = games.Text(self, x, y, f'MP: {self.avatar.get_mp()}/{self.avatar.get_max_mp()}', int(BOX_SIZE*1.5), color.BLUE)
                x, y = self.cell_to_coords(WIDTH//6+2,HEIGHT//2-0.5)
                self.myLVLText = games.Text(self, x, y, f'Lvl. {self.lvl}', int(BOX_SIZE*1.5), color.PURPLE)
                x, y = self.cell_to_coords(WIDTH-6,1.5)
                self.enemyLVLText = games.Text(self, x, y, f'Lvl. {self.elvl}', int(BOX_SIZE*1.5), color.PURPLE)
                x, y = self.cell_to_coords(WIDTH-6,0.5)
                self.enemyHPText = games.Text(self, x, y, f'HP: {self.enemy.get_hp()}/{self.enemy.get_max_hp()}', int(BOX_SIZE*1.5), color.RED)


    def mouse_up(self, xy, button):
        x, y = xy[0], xy[1]
        i, j = self.coords_to_cell(x, y)
        if self.on_board(i, j):
            if str(type(self.grid[i][j])) in ["<class '__main__.BButton'>","<class '__main__.OButton'>"]:
                self.grid[i][j].action()

    def level_up(self):
        while self.exp >= self.max_exp:
            if self.lvl == 100:
                return None
            self.lvl += 1
            self.exp -= self.max_exp
            self.max_exp += 2
            self.avatar.max_hp += randint(1,5)
            magic_number = randint(1,3)
            self.magic_power += magic_number
            self.avatar.max_mp += magic_number
            atk = randint(1,3)
            self.avatar.atkl += atk
            self.avatar.atkh += atk
            self.avatar.df += randint(1,3)




    def keypress(self, key):
        if self.randEncounter:
            return None
        if self.battle:
            self.battleKey(key)
        elif self.bag_menu:
            self.bagKey(key)
        else:
            self.worldKey(key)

    def worldKey(self, key):
        if key in [97,boards.K_LEFT]:
            self.player.Xmove(-1)
        elif key in [100,boards.K_RIGHT]:
            self.player.Xmove(1)
        elif key in [119,boards.K_UP]:
            self.player.Ymove(-1)
        elif key in [115,boards.K_DOWN]:
            self.player.Ymove(1)
        elif key in [98,101]:
            self.load_bag()

    def updateBattle(self):
        if self.enemy.get_hp() == 0:
            self.battle = False
            if self.enemyType == RICK_ASTLEY:
                self.meme.stop()
            elif self.enemyType == SANS:
                self.megalovania.stop()
            elif self.enemyType > NUM_NORMAL_ENEMIES:
                self.bossMusic.stop()
            else:
                self.battleMusic.stop()
            self.overworldMusic.play()
            self.avatar.heal(self.avatar.get_max_hp()//5)
            for item in self.myItems:
                if randint(1,5) == 1:
                    item["quantity"] += 1
            if self.enemyType > NUM_NORMAL_ENEMIES:
                self.bossesBeaten += 1
            self.enemyHPText.destroy()
            self.enemyLVLText.destroy()
            self.myHPText.destroy()
            self.myMPText.destroy()
            self.myLVLText.destroy()
            for i in self.grid:
                for j in i:
                    try:
                        j.label.destroy()
                    except:
                        pass
            self.loadchunk(self.chunkcoords[0],self.chunkcoords[1])
            if self.world[self.player.geoY][self.player.geoX] == 'B':
                self.player.remove_item()
            self.move_player(self.player.x, self.player.y)
            if (self.exp, self.lvl) == (0,5):
                self.tip_display('You Won Your First Battle!')
            self.exp += self.elvl + randint(0,5)
            self.level_up()
            if self.bossesBeaten == NUM_BOSSES:
                self.bossesBeaten -= 1
                self.tip_display('YOU BEAT THE GAME!')
        else:
            damage = self.enemy.generate_damage()
            damage = int(damage*(0.99**self.avatar.df))
            if damage < 1:
                damage = 1
            self.avatar.take_damage(damage)
            if self.avatar.get_hp() == 0:
                self.encounterTicker = 8
                self.chunkcoords = [1,1]
                self.battle = False
                for item in self.myItems:
                    item["quantity"] = randint(0, item["quantity"])
                if self.enemyType == RICK_ASTLEY:
                    self.meme.stop()
                elif self.enemyType == SANS:
                    self.megalovania.stop()
                elif self.enemyType > NUM_NORMAL_ENEMIES:
                    self.bossMusic.stop()
                else:
                    self.battleMusic.stop()
                self.overworldMusic.play()
                self.avatar.heal(self.avatar.get_max_hp())
                self.avatar.mp = self.avatar.max_mp
                self.enemyHPText.destroy()
                self.enemyLVLText.destroy()
                self.myHPText.destroy()
                self.myMPText.destroy()
                self.myLVLText.destroy()
                for i in self.grid:
                    for j in i:
                        try:
                            j.label.destroy()
                        except:
                            pass
                self.player.teleport(self.spawnpoint[0], self.spawnpoint[1])
                self.tip_display('YOU DIED')
            else:
                self.replace_text()
                self.myMessage += f', Enemy Dealt {damage} Damage'
                x, y = self.comment.pos()
                self.comment.destroy()
                self.comment = games.Text(self, x, y, self.myMessage, TEXT_SIZE, color.WHITE)

                


    def battleKey(self, key):
        if key == 49:
            pass

    def bagKey(self, key):
        if key in [98, 101]:
            self.close_bag()


class Square(boards.GameCell):
    
    def __init__(self, board, i, j):
        self.init_tile(board, i, j)

    def init_tile(self, board, i, j):
        self.board = board
        self.init_gamecell(board, i, j)

    def step_action(self):
        pass

    def hoverAction(self):
        pass

class Water(Square):
    def __init__(self, board, i, j):
        self.init_tile(board, i, j)
        self.tileColor = color.LIGHTBLUE
        self.set_color(self.tileColor)

    def step_action(self):
        if randint(1,2*ENCOUNTER)==1 and self.board.player.encounterTicker == 0:
            self.board.encounter()
            self.board.player.encounterTicker = 8

class Sand(Square):
    def __init__(self, board, i, j):
        self.init_tile(board, i, j)
        self.tileColor = [(219,204,125),(179,165,96)][randint(0,1)]
        self.set_color(self.tileColor)

    def step_action(self):
        if randint(1,ENCOUNTER)==1 and self.board.player.encounterTicker == 0:
            self.board.encounter()
            self.board.player.encounterTicker = 8

class Grass(Square):
    def __init__(self, board, i, j):
        self.init_tile(board, i, j)
        self.tileColor = [(71,194,14),(86,237,17)][randint(0,1)]
        self.set_color(self.tileColor)

    def step_action(self):
        if randint(1,ENCOUNTER)==1 and self.board.player.encounterTicker == 0:
            self.board.encounter()
            self.board.player.encounterTicker = 8

class Mud(Square):
    def __init__(self, board, i, j):
        self.init_tile(board, i, j)
        self.tileColor = [(54,41,19),(70,60,31)][randint(0,1)]
        self.set_color(self.tileColor)

    def step_action(self):
        if randint(1,ENCOUNTER)==1 and self.board.player.encounterTicker == 0:
            self.board.encounter()
            self.board.player.encounterTicker = 8

class Plank(Square):
    def __init__(self, board, i, j):
        self.init_tile(board, i, j)
        self.tileColor = [(184, 145, 79),(153,120,63)][randint(0,1)]
        self.set_color(self.tileColor)

class Wall(Square):
    def __init__(self, board, i, j):
        self.init_tile(board, i, j)
        self.tileColor = [color.DARKGRAY,color.GRAY][randint(0,1)]
        self.set_color(self.tileColor)

class Checkpoint(Square):
    def __init__(self, board, i, j):
        self.init_tile(board, i, j)
        self.tileColor = (27, 0, 125)
        self.set_color(self.tileColor)

    def step_action(self):
        self.board.spawnpoint[0] = self.board.player.geoX
        self.board.spawnpoint[1] = self.board.player.geoY
        self.board.save_game()
        self.board.tip_display('GAME SAVED')

class Teleporter(Square):
    def __init__(self, board, i, j):
        self.init_tile(board, i, j)
        self.tileColor = color.PURPLE
        self.set_color(self.tileColor)
        self.tpID = self.board.world[j+HEIGHT*(self.board.chunkcoords[1]-1)][i+WIDTH*(self.board.chunkcoords[0]-1)]

    def step_action(self):
        x, y = -1, -1
        for j in self.board.world:
            y += 1
            for i in j:
                x += 1
                if (x,y) == (self.board.player.geoX,self.board.player.geoY):
                    continue
                if i == self.tpID:
                    self.board.player.teleport(x, y)
                    return None
            x = -1

class Raft(Square):
    def __init__(self, board, i, j):
        self.init_tile(board, i, j)
        self.tileColor = color.ORANGE
        if ' ' in self.board.walkableTiles:
            self.tileColor = [(71,194,14),(86,237,17)][randint(0,1)]
        self.set_color(self.tileColor)

    def step_action(self):
        if not ' ' in self.board.walkableTiles:
            self.board.walkableTiles.append(' ')
            self.tileColor = [(71,194,14),(86,237,17)][randint(0,1)]
            self.board.tip_display('You Got A Raft! You Can Use It To Travel Across Water!')
            self.board.player.remove_item()

class WorldItem(Square):
    def __init__(self, board, i, j):
        self.init_tile(board, i, j)
        self.tileColor = color.GOLD
        self.set_color(self.tileColor)

    def step_action(self):
        item = self.board.myItems[randint(0,len(self.board.myItems)-1)]
        boost = randint(2,5)
        item["quantity"] += boost
        self.board.tip_display(f'You Got {boost} {item["item"].name}s!')
        self.board.player.remove_item()

class Boss(Square):
    def __init__(self, board, i, j):
        self.init_tile(board, i, j)
        self.tileColor = (200, 0, 0)
        self.set_color(self.tileColor)

    def step_action(self):
        self.board.encounter()
        self.board.player.encounterTicker = 8

class OBar(Square):
    def __init__(self, board, i, j):
        self.init_tile(board, i, j)
        self.tileColor = color.BLACK
        self.set_color(self.tileColor)

class OMenu(Square):
    def __init__(self, board, i, j):
        self.init_tile(board, i, j)
        self.tileColor = (0,10,120)
        self.set_color(self.tileColor)

    def hoverAction(self):
        x, y = self.board.bag_description.pos()
        self.board.bag_description.destroy()
        self.board.bag_description = games.Text(self.board, x, y, '', TEXT_SIZE, color.WHITE)

class OButton(Square):
    def __init__(self, board, i, j, myID):
        self.ID = myID
        self.init_tile(board, i, j)
        self.board.oldMousePos = None
        self.tileColor = color.WHITE
        self.set_color(self.tileColor)
        self.x = i-0.5
        self.y = j+0.5
        x, y = self.board.cell_to_coords(self.x,self.y)
        self.label = games.Text(self.board, x, y, BAG_MENU_OPTIONS[self.board.blayer][self.ID-1], TEXT_SIZE, color.WHITE)
        self.replace_text(BAG_MENU_OPTIONS[self.board.blayer][self.ID-1], x, y)

    def replace_text(self,theStr,oldx,oldy):
        self.label.destroy()
        x, y = oldx, oldy
        self.label = games.Text(self.board, x, y, theStr, TEXT_SIZE, color.WHITE)
        width = self.label.bbox()[2]
        x -= width/2
        self.label.destroy()
        self.label = games.Text(self.board, x, y, theStr, TEXT_SIZE, color.WHITE)


    def action(self):
        if self.board.blayer == 1 and self.ID == 1:
            self.board.load_blayer_options(2)
        elif self.board.blayer == 1 and self.ID == 2:
            self.board.load_blayer_options(3)
        elif self.board.blayer == 2 and self.ID == 4:
            self.board.load_blayer_options(1)
        elif self.board.blayer == 3 and self.ID == 4:
            self.board.load_blayer_options(1)
        elif self.ID == 3 and self.board.blayer == 1:
            spell = self.board.avatar.magic[1]
            if spell.cost <= self.board.avatar.get_mp():
                self.board.avatar.reduce_mp(spell.cost)
                self.board.avatar.heal(spell.dmg)
                self.board.replace_bag_text()
        elif self.board.blayer == 2:
            item = self.board.myItems[self.ID-1]
            if item["quantity"] > 0:
                item["quantity"] -= 1
                self.board.avatar.heal(item["item"].prop)
                self.board.load_blayer_options(2)
                self.board.replace_bag_text()
        elif self.board.blayer == 3:
            item = self.board.myItems[self.ID+2]
            if item["quantity"] > 0:
                item["quantity"] -= 1
                self.board.avatar.mp += item["item"].prop
                if self.board.avatar.mp > self.board.avatar.max_mp:
                    self.board.avatar.mp = self.board.avatar.max_mp
                self.board.load_blayer_options(3)
                self.board.replace_bag_text()
        elif self.board.blayer == 1 and self.ID == 4:
            self.board.close_bag()

    
    def hoverAction(self):
        x, y = self.board.bag_description.pos()
        self.board.bag_description.destroy()
        self.board.bag_description = games.Text(self.board, x, y, BAG_DESCRIPTIONS[self.board.blayer-1][self.ID-1], TEXT_SIZE, color.WHITE)

class BattleBlock(Square):
    def __init__(self, board, i, j):
        self.init_tile(board, i, j)

class BBar(BattleBlock):
    def __init__(self, board, i, j):
        self.init_tile(board, i, j)
        self.tileColor = color.BLACK
        self.set_color(self.tileColor)

class BBackground(BattleBlock):
    def __init__(self, board, i, j):
        self.init_tile(board, i, j)
        self.tileColor = (219,219,219)
        self.set_color(self.tileColor)

class BPlayer(BattleBlock):
    def __init__(self, board, i, j):
        self.init_tile(board, i, j)
        self.tileColor = color.RED
        self.set_color(self.tileColor)

class BEnemy(BattleBlock):
    def __init__(self, board, i, j):
        self.init_tile(board, i, j)
        x, y = (i-WIDTH+10,j-2)
        self.tileColor = LETTERCOLORS[self.board.enemyArt[y][x]]
        self.set_color(self.tileColor)

class BMenu(BattleBlock):
    def __init__(self, board, i, j):
        self.init_tile(board, i, j)
        self.tileColor = (0,10,120)
        self.set_color(self.tileColor)

    def hoverAction(self):
        x, y = self.board.description.pos()
        self.board.description.destroy()
        self.board.description = games.Text(self.board, x, y, '', TEXT_SIZE, color.WHITE)

class BButton(BattleBlock):
    def __init__(self, board, i, j):
        self.init_tile(board, i, j)
        self.board.oldMousePos = None
        self.tileColor = color.WHITE
        self.set_color(self.tileColor)
        self.ID = 1
        if (i,j) == (2*WIDTH//3,5*(HEIGHT//6)-1):
            self.ID = 2
        elif (i,j) == (WIDTH//4,5*(HEIGHT//6)+1):
            self.ID = 3
        elif (i,j) == (2*WIDTH//3,5*(HEIGHT//6)+1):
            self.ID = 4
        self.x = i-0.5
        self.y = j+0.5
        x, y = self.board.cell_to_coords(self.x,self.y)
        self.label = games.Text(self.board, x, y, MENU_OPTIONS[self.board.layer][self.ID-1], TEXT_SIZE, color.WHITE)
        self.replace_text(MENU_OPTIONS[self.board.layer][self.ID-1], x, y)

    def replace_text(self,theStr,oldx,oldy):
        self.label.destroy()
        x, y = oldx, oldy
        self.label = games.Text(self.board, x, y, theStr, TEXT_SIZE, color.WHITE)
        width = self.label.bbox()[2]
        x -= width/2
        self.label.destroy()
        self.label = games.Text(self.board, x, y, theStr, TEXT_SIZE, color.WHITE)

    def action(self):
        x, y = self.board.comment.pos()
        self.board.comment.destroy()
        self.board.comment = games.Text(self.board, x, y, '', TEXT_SIZE, color.WHITE)
        if self.ID == 4 and self.board.layer == 1:
            if self.board.enemyType > NUM_NORMAL_ENEMIES:
                return None
            if randint(0,1)==0:
                self.board.myMessage = 'You Failed To Run'
                self.board.updateBattle()
                return None
            self.board.battle = False
            if self.board.enemyType == RICK_ASTLEY:
                self.board.meme.stop()
            elif self.board.enemyType == SANS:
                self.board.megalovania.stop()
            elif self.board.enemyType > NUM_NORMAL_ENEMIES:
                self.board.bossMusic.stop()
            else:
                self.board.battleMusic.stop()
            self.board.overworldMusic.play()
            self.board.enemyHPText.destroy()
            self.board.enemyLVLText.destroy()
            self.board.myHPText.destroy()
            self.board.myMPText.destroy()
            self.board.myLVLText.destroy()
            for i in self.board.grid:
                for j in i:
                    try:
                        j.label.destroy()
                    except:
                        pass
            self.board.loadchunk(self.board.chunkcoords[0],self.board.chunkcoords[1])
            self.board.move_player(self.board.player.x, self.board.player.y)
        elif self.ID == 1 and self.board.layer == 1:
            damage = self.board.avatar.generate_damage()
            self.board.enemy.take_damage(damage)
            MPBoost = randint(1,self.board.avatar.max_mp//4)
            self.board.myMessage = f'You Dealt {damage} Damage And Regained {MPBoost} MP'
            self.board.avatar.mp += MPBoost
            if self.board.avatar.mp > self.board.avatar.max_mp:
                self.board.avatar.mp = self.board.avatar.max_mp
            self.board.updateBattle()
        elif self.ID == 2 and self.board.layer == 1:
            self.board.load_layer_options(3)
        elif self.ID == 4 and self.board.layer == 3:
            self.board.load_layer_options(1)
        elif self.ID == 1 and self.board.layer == 3:
            self.board.load_layer_options(4)
        elif self.ID == 2 and self.board.layer == 3:
            self.board.load_layer_options(5)
        elif self.ID == 3 and self.board.layer == 3:
            self.board.load_layer_options(6)
        elif self.ID == 4 and self.board.layer in [4,5,6]:
            self.board.load_layer_options(3)
        elif self.ID == 3 and self.board.layer == 1:
            self.board.load_layer_options(2)
        elif self.ID == 4 and self.board.layer == 2:
            self.board.load_layer_options(1)
        elif self.ID == 1 and self.board.layer == 2:
            spell = self.board.avatar.magic[0]
            damage = spell.generate_damage()+self.board.magic_power
            if spell.cost <= self.board.avatar.get_mp():
                self.board.avatar.reduce_mp(spell.cost)
                self.board.enemy.take_damage(damage)
                self.board.load_layer_options(1)
                self.board.myMessage = f'You Dealt {damage} Damage'
                self.board.updateBattle()
        elif self.ID == 2 and self.board.layer == 2:
            spell = self.board.avatar.magic[1]
            if spell.cost <= self.board.avatar.get_mp():
                self.board.avatar.reduce_mp(spell.cost)
                self.board.avatar.heal(spell.dmg)
                self.board.load_layer_options(1)
                self.board.myMessage = 'You Healed All Damage'
                self.board.updateBattle()
        elif self.ID == 3 and self.board.layer == 2:
            spell = self.board.avatar.magic[2]
            damage = spell.generate_damage()+self.board.magic_power
            if spell.cost <= self.board.avatar.get_mp():
                self.board.avatar.reduce_mp(spell.cost)
                self.board.enemy.take_damage(damage)
                self.board.avatar.heal(damage//2)
                self.board.load_layer_options(1)
                self.board.myMessage = f'You Dealt {damage} Damage And Stole {damage//2} HP'
                self.board.updateBattle() 
        elif self.board.layer == 4:
            item = self.board.myItems[self.ID-1]
            if item["quantity"] > 0:
                item["quantity"] -= 1
                self.board.avatar.heal(item["item"].prop)
                self.board.load_layer_options(1)
                self.board.myMessage = f'You Healed {item["item"].prop} HP'
                if item["item"].prop == 9999:
                    self.board.myMessage = 'You Healed All Damage'
                self.board.updateBattle() 
        elif self.board.layer == 5:
            item = self.board.myItems[self.ID+2]
            if item["quantity"] > 0:
                item["quantity"] -= 1
                self.board.avatar.mp += item["item"].prop
                if self.board.avatar.mp > self.board.avatar.max_mp:
                    self.board.avatar.mp = self.board.avatar.max_mp
                self.board.load_layer_options(1)
                self.board.myMessage = f'You Regained {item["item"].prop} MP'
                if item["item"].prop == 9999:
                    self.board.myMessage = 'You Regained All MP'
                self.board.updateBattle()
        elif self.board.layer == 6:
            item = self.board.myItems[self.ID+5]
            if item["quantity"] > 0:
                item["quantity"] -= 1
                self.board.enemy.take_damage(item["item"].prop)
                self.board.load_layer_options(1)
                self.board.myMessage = f'You Dealt {item["item"].prop} Damage'
                self.board.updateBattle()

            


                

    def hoverAction(self):
        self.board.description.destroy()
        x, y = self.board.cell_to_coords(WIDTH/2, HEIGHT-1)
        myText = DESCRIPTIONS[self.board.layer-1][self.ID-1]
        if self.ID == 4 and self.board.layer == 1 and self.board.enemyType > NUM_NORMAL_ENEMIES:
            myText = 'You Can\'t Run From Boss Battles!'
        self.board.description = games.Text(self.board, x, y, myText, TEXT_SIZE, color.WHITE)


class Player:
    def __init__(self, board):
        self.x = WIDTH//2
        self.y = HEIGHT//2
        self.geoX = self.x
        self.geoY = self.y
        self.encounterTicker = 8
        self.board = board

    def remove_item(self):
        world = self.board.world
        x, y = (self.geoX, self.geoY)
        if len(world)>(y+1):
            newTile = world[y+1][x]
        else:
            newTile = world[y-1][x]
        self.board.world[y] = world[y][:x]+newTile+world[y][(x+1):]
        self.board.chunk[self.y] = self.board.chunk[self.y][:self.x]+newTile+self.board.chunk[self.y][(self.x+1):]
        self.board.grid[self.x][self.y].destroy()
        self.board.grid[self.x][self.y] = self.board.new_gamecell(self.x, self.y)
        self.board.move_player(self.x, self.y)

    def Xmove(self,distance):
        newX = self.x+distance
        if self.geoX + distance >= len(self.board.world[self.geoY]) or self.geoX + distance < 0:
            return None
        if not self.board.world[self.geoY][self.geoX+distance] in self.board.walkableTiles:
            return None
        if newX >= WIDTH:
            newX = 0
            self.board.chunkcoords = [self.board.chunkcoords[0]+1,self.board.chunkcoords[1]]
            self.board.loadchunk(self.board.chunkcoords[0],self.board.chunkcoords[1])
        elif newX < 0:
            newX = WIDTH-1
            self.board.chunkcoords = [self.board.chunkcoords[0]-1,self.board.chunkcoords[1]]
            self.board.loadchunk(self.board.chunkcoords[0],self.board.chunkcoords[1])
        self.board.move_player(newX, self.y)
        self.x = newX
        self.geoX += distance
        self.board.grid[self.x][self.y].step_action()
        if self.encounterTicker > 0:
            self.encounterTicker -= 1

    def Ymove(self,distance):
        newY = self.y+distance
        if self.geoY + distance >= len(self.board.world) or self.geoY + distance < 0:
            return None
        if not self.board.world[self.geoY+distance][self.geoX] in self.board.walkableTiles:
            return None
        if newY >= HEIGHT:
            newY = 0
            self.board.chunkcoords = [self.board.chunkcoords[0],self.board.chunkcoords[1]+1]
            self.board.loadchunk(self.board.chunkcoords[0],self.board.chunkcoords[1])
        elif newY < 0:
            newY = HEIGHT-1
            self.board.chunkcoords = [self.board.chunkcoords[0],self.board.chunkcoords[1]-1]
            self.board.loadchunk(self.board.chunkcoords[0],self.board.chunkcoords[1])
        self.board.move_player(self.x, newY)
        self.y = newY
        self.geoY += distance
        self.board.grid[self.x][self.y].step_action()
        if self.encounterTicker > 0:
            self.encounterTicker -= 1

    def teleport(self, x, y):
        self.geoX = x
        self.geoY = y
        self.board.chunkcoords = [x//WIDTH+1, y//HEIGHT+1]
        self.x = x - WIDTH*(self.board.chunkcoords[0]-1)
        self.y = y - HEIGHT*(self.board.chunkcoords[1]-1)
        self.board.loadchunk(self.board.chunkcoords[0],self.board.chunkcoords[1])
        self.board.move_player(self.x, self.y)
#---------------------------------






game = RPGgame()
game.mainloop()

