from gasp import games
from gasp import color
from gasp import boards

BOX_SIZE = 40
MARGIN = 80
WIDTH = 8
HEIGHT = 8
TEXT_SIZE = 30
ABET = 'abcdefghijklmnopqrstuvwxyz'

class Test(boards.SingleBoard):
    def __init__(self):
        self.init_singleboard((MARGIN, MARGIN), WIDTH, HEIGHT, BOX_SIZE)
        self.draw_all_outlines()
        x, y = self.cell_to_coords(WIDTH//2, HEIGHT//2)
        self.testText = games.Text(self, x, y, 'Test', TEXT_SIZE, color.WHITE)
        shove = x - self.testText.bbox()[0]
        self.testText.move_by(shove,0)

    def replace_text(self,theStr):
        self.testText.destroy()
        x, y = self.cell_to_coords(WIDTH//2, HEIGHT//2)
        self.testText = games.Text(self, x, y, theStr, TEXT_SIZE, color.WHITE)
        width = self.testText.bbox()[2]
        x -= width/2
        self.testText.destroy()
        self.testText = games.Text(self, x, y, theStr, TEXT_SIZE, color.WHITE)

    def keypress(self, key):

        if key == 1073742049:
            self.testText.set_text('Bigger Text')
        elif key == 1073742053:
            self.testText.set_text('a')
        elif key >= 97 and key <= 122:
            self.replace_text(self.testText.get_text()+ABET[key-97])
        elif key == 8:
            self.replace_text(self.testText.get_text()[:-1])
        elif key == 32:
            self.testText.move_to(20,20)
        print(key)

    def new_gamecell(self, i, j):
        return Square(self, i, j)

class Square(boards.GameCell):
    def __init__(self, board, i, j):
        self.init_gamecell(board, i, j)

game = Test()
game.mainloop()
