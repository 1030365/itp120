step = 30
handle = open('WORLD.txt')
world = []
for line in handle:
    world.append(line[:-1])
while not len(world)//step == len(world)/step:
    world.append('')
geowidth = 0
for line in world:
    if len(line)>geowidth:
        geowidth = len(line)
if not geowidth//step == geowidth/step:
    geowidth = step*(geowidth//step+1)
j = -1
for line in world:
    j+=1
    length = len(world[j])
    while not length == geowidth:
        world[j] += '+'
        length = len(world[j])

for line in world:
    print(line)
