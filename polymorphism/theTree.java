abstract class consoles {
    public void game() {}
}

class WiiU extends consoles{
    public void game() {
        System.out.println("Mario Kart 8");
    }
}

class Switch extends consoles{
    public void game() {
        System.out.println("Breath of the Wild");
    }
}

class theGames {
    public static void main(String[] args) {
        consoles c;
        c = new WiiU();
        c.game();
        c = new Switch();
        c.game();
    }
}
