class giveExamps {
    public static void main (String[] args) {
        Dog doga = new Dog();
        Dog dogb = new Dog();
        Dog dogc = doga;
        boolean sameDog = (doga.equals(dogc));
        int a = 3;
        byte b = 3;
        boolean sameNum = (a == b);
        System.out.println(sameDog);
        System.out.println(sameNum);
        System.out.println(doga.hashCode());
    }
}

class Dog {
    public void bark () {
        System.out.println("woof");
    }
}
