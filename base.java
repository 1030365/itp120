import java.util.*;
import java.awt.event.*;
import javax.swing.*;
import java.awt.*;
import java.io.*;

class ConvertToBase {
    private JFrame frame;
    private JTextArea num;
    private JTextArea converted;
    private JTextArea baseNum;

    //String containing Hex digits for all nums in order
    String abetnums = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";

    public static void main (String[] args) {
        ConvertToBase converter = new ConvertToBase();
        converter.go();
    }

    public void go() {

        //Gui magic
        frame = new JFrame("Base Converter");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        JPanel mainPanel = new JPanel();
        Font bigFont = new Font("sanserif", Font.BOLD, 24);
        
        //add num text box
        num = new JTextArea(6,20);
        num.setLineWrap(true); 
        num.setWrapStyleWord(true); 
        num.setFont(bigFont);
        mainPanel.add(num);
        
        //add base num text box
        baseNum = new JTextArea(3,10);
        baseNum.setLineWrap(true);
        baseNum.setWrapStyleWord(true);
        baseNum.setFont(bigFont);
        mainPanel.add(baseNum);
        
        //add converter button
        JButton converter = new JButton("Convert");
        converter.addActionListener(new ConvertListener());
        mainPanel.add(converter);
        
        //add converted text box
        converted = new JTextArea(3,10);
        converted.setLineWrap(true);
        converted.setWrapStyleWord(true);
        converted.setFont(bigFont);
        mainPanel.add(converted);
        
        //anticonverter button
        JButton anticonvert = new JButton("Convert But Anti");
        anticonvert.addActionListener(new ReverseConvertListener());
        mainPanel.add(anticonvert);
        
        //display GUI
        frame.getContentPane().add(BorderLayout.CENTER, mainPanel);
        frame.setSize(500,600);
        frame.setVisible(true);
    }
    public class ConvertListener implements ActionListener {
        public void actionPerformed(ActionEvent ev) {
            int bNum = Integer.parseInt(num.getText());
            String c = "";
            int r;
            int Base = Integer.parseInt(baseNum.getText());

            while (bNum > 0) {

                //find remainder of bNum/Base
                r = bNum % Base;

                //get number OR letter that corresponds to remainder
                c = abetnums.substring(r,r+1) + c;

                //remove the remainder from the total number and divide number by the base
                bNum -= r;
                bNum = bNum/Base;
            }
            converted.setText(c);
        }
    }
    public class ReverseConvertListener implements ActionListener {
        public void actionPerformed(ActionEvent ev) {
            int Base = Integer.parseInt(baseNum.getText());
            String bStr = converted.getText();
            int numlength = bStr.length();
            int c = 0; //will become final decimal number
            int x;

            //for loop that runs once per digit of input number
            for (int i=0; i<numlength; i++) {

                //retreive numbers from right to left
                x = abetnums.indexOf(bStr.substring(numlength-i-1,numlength-i));

                //add the number times the weight to c
                c += x*Math.pow(Base,i);
            }
            num.setText(Integer.toString(c));
        }
    }
}
