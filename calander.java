import java.util.*;
import java.io.*;
class Watch {
    public static void main(String[] args) {
        try {
            PrintStream fileOut = new PrintStream("./date.txt");
            PrintStream fileErr = new PrintStream("./date.txt");
            System.setOut(fileOut);
            System.setErr(fileOut);
        }catch(FileNotFoundException ex) {
            ex.printStackTrace();
        }
        Calendar c = Calendar.getInstance();
        long day1 = c.getTimeInMillis();
        System.out.println(c.getTime());
        day1 += 1000*60*60*5;
        c.setTimeInMillis(day1);
        System.out.println("5 Hours Added: "+c.getTime());
        c.add(c.HOUR, 10);
        System.out.println("2 More Hours Added: "+c.getTime());
        c.roll(c.HOUR, 23);
        System.out.println("Roll 23 hours: "+c.getTime());
        System.out.println("Date: "+(1+c.get(c.MONTH))+"/"+c.get(c.DATE)+"/"+c.get(c.YEAR));
    }
}
