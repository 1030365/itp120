class Overloader {
    public static void main(String[] args) {
        identify("yay");
        identify("oof");
        identify(2);
        identify(true);
    }
    public static void identify(String theText) {
        System.out.println(theText);
    }
    public static void identify(int theInt) {
        System.out.println(theInt);
    }
    public static void identify(boolean theBoolean) {
        System.out.println(theBoolean);
    }
}
