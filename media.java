import javax.sound.midi.*;
class MiniMusicCmdLine {
    public static void main(String[] args) {
        MiniMusicCmdLine mini = new MiniMusicCmdLine();
        //notify the user if there aren't enough arguments
        if (args.length < 4) {
            System.out.println("Don't forget the instrument, note args, repeats, and increment");
        } else {
            //retreive integers
            int instrument = Integer.parseInt(args[0]);
            int note = Integer.parseInt(args[1]);
            int reps = Integer.parseInt(args[2]);//parse reps argument into int
            int incr = Integer.parseInt(args[3]);//parse incr argument into int
            //play sound reps amount of times, incrementing the instrument number by incr each time.
            for (int x=0;x<reps;x++) {
                mini.play(instrument, note);
                //if incrementing the instrument won't send instrument out of range, add incr to instrument
                if (instrument+incr<128) {
                    instrument += incr;
                } else {//roll back to 0 and add remaining incr if instrument goes above 127
                    instrument = (instrument+incr)-128;
                }
            }
            //kill program after final note finishes
            System.exit(0);
        }
    }
    public void play(int instrument, int note) {
        try {
            Sequencer player = MidiSystem.getSequencer();
            player.open();
            Sequence seq = new Sequence(Sequence.PPQ, 4);
            Track track = seq.createTrack();
            MidiEvent event = null;
            ShortMessage first = new ShortMessage();
            first.setMessage(192, 1, instrument, 0);
            MidiEvent changeInstrument = new MidiEvent(first, 1);
            track.add(changeInstrument);
            ShortMessage a = new ShortMessage();
            a.setMessage(144, 1, note, 100);
            MidiEvent noteOn = new MidiEvent(a, 1);
            track.add(noteOn);
            ShortMessage b = new ShortMessage();
            b.setMessage(128, 1, note, 100);
            MidiEvent noteOff = new MidiEvent(b, 16);
            track.add(noteOff);
            player.setSequence(seq);
            player.start();
            //wait 2 seconds after note plays before ending function
            Thread.sleep(2000);
        } catch (Exception ex) {ex.printStackTrace();}
    }
}
