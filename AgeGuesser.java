import java.io.*;

class CliReader {
    public String input(String prompt) {
        String inputLine = null;
        System.out.print(prompt + " ");
        try {
            BufferedReader is = new BufferedReader(
                new InputStreamReader(System.in)
            );
            inputLine = is.readLine();
            if (inputLine.length() == 0) return null;
        }
        catch (IOException e) {
            System.out.println("IOException: " + e);
        }
        return inputLine;
    }
}

class WhoDriver {
    public static void guessAge(Who sally) {
        String info;
        String message = "";
        CliReader reader = new CliReader();

        System.out.println("Time to guess " + sally.getMyName() + "'s age.");
        while (message != "You guessed it!") {
            info = reader.input("How old do you think I am? ");
            message = sally.howOld(Integer.parseInt(info));
            System.out.println(message);
        }
        System.out.println("You took " + sally.getGuessCount() + " guesses.\n");
    }

    public static void main(String[] args) {
        Who thing1 = new Who("Thing 1");
        Who thing2 = new Who("Thing 2");

        System.out.println(thing1.sayHi());
        System.out.println(thing2.sayHi());

        guessAge(thing1);
        guessAge(thing2);
    }
}

class Who {
    private String myName;
    private int myAge = (int)(Math.random() * 10) + 10;
    private int ageGuessCount = 0;

    Who(String name) {
        myName = name;
    }

    public String sayHi() {
        return "my name is " + myName + " and I just want to say,\nthat the" + " thing I like most is to play, play, play, play!\n";
    }

    public String howOld(int guess) {
        this.ageGuessCount++;

        if (guess > myAge)
            return "I'm not that old.";
        if (guess < myAge)
            return "I'm older than that.";
        return "You guessed it!";
    }
    
    public int getGuessCount() {
        return this.ageGuessCount;
    }

    public String getMyName() {
        return this.myName;
    }
}
