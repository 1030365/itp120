import java.awt.Color;
import java.awt.Graphics;
import javax.swing.JPanel;
import javax.swing.JFrame;

//Create Smiley Face
class DrawSmiley extends JPanel {
    public void paintComponent(Graphics g) {
        super.paintComponent(g);
        
        //randomize colors
        Color ACOLOR = new Color((int) (Math.random() * 256),(int) (Math.random() * 256),(int) (Math.random() * 256));
        Color BCOLOR = new Color((int) (Math.random() * 256),(int) (Math.random() * 256),(int) (Math.random() * 256));
        

        //draw face
        g.setColor(ACOLOR);//Use color A
        g.fillOval(10, 10, 200, 200);

        //draw eyes
        g.setColor(BCOLOR);//use color B
        g.fillOval(50,65,30,30);
        g.fillOval(135,65,30,30);

        //Draw mouth
        g.fillOval(50, 110, 120, 60);

        //Make mouth into smile
        g.setColor(ACOLOR);//Use color A again
        g.fillRect(50,110,120,30);
        g.fillOval(50,120,120,40);
    }
}

class DrawSmileyTest {
    public static void main(String[] args) {
        DrawSmiley panel = new DrawSmiley();
        JFrame application = new JFrame();

        application.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        application.add(panel);
        application.setSize(230,250);
        application.setVisible(true);
        //repeat infinitely
        while (true) {
            //Create new Random Smiley
            panel = new DrawSmiley();

            //Place new random smiley
            application.add(panel);
            application.setVisible(true);

            //repeat after every 1 second
            try {
                Thread.sleep(1000);
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
    }
}
