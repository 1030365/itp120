import java.io.*; 
import java.net.*; 
import java.util.*;
import javax.swing.*; 
import java.awt.*; 
import java.awt.event.*;

class SimpleChatClient {
    JTextArea incoming; 
    JTextField outgoing; 
    BufferedReader reader; 
    PrintWriter writer; 
    Socket sock;

    public static void main(String[] args) { 
        SimpleChatClient client = new SimpleChatClient();
        client.go();
    }

    public void go() {
        //gui
        JFrame frame = new JFrame("Ludicrously Simple Chat Client"); 
        JPanel mainPanel = new JPanel();
        incoming = new JTextArea(15,50); 
        incoming.setLineWrap(true); 
        incoming.setWrapStyleWord(true); 
        incoming.setEditable(false);
        JScrollPane qScroller = new JScrollPane(incoming); 
        qScroller.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS); 
        qScroller.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
        outgoing = new JTextField(20);
        JButton sendButton = new JButton("Send"); 
        sendButton.addActionListener(new SendButtonListener()); 
        mainPanel.add(qScroller);
        mainPanel.add(outgoing);
        mainPanel.add(sendButton);

        //not gui, running main code
        setUpNetworking();
        Thread readerThread = new Thread(new IncomingReader()); 
        readerThread.start();

        //gui
        frame.getContentPane().add(BorderLayout.CENTER, mainPanel); 
        frame.setSize(800,500);
        frame.setVisible(true);
    } // close go

    //connects to socket
    private void setUpNetworking() {
        try {
            sock = new Socket("127.0.0.1", 1985);
            InputStreamReader streamReader = new InputStreamReader(sock.getInputStream()); 
            reader = new BufferedReader(streamReader);
            writer = new PrintWriter(sock.getOutputStream()); 
            System.out.println("networking established");
        } catch(IOException ex) {
            ex.printStackTrace(); 
        }
    } // close setUpNetworking

    //sends data to server
    public class SendButtonListener implements ActionListener { 
        public void actionPerformed(ActionEvent ev) {
            try { 
                writer.println(outgoing.getText()); 
                writer.flush();
            } catch(Exception ex) {
                ex.printStackTrace();
            }
            outgoing.setText("");
            outgoing.requestFocus();
        }
    } // close inner class

    //retreives readable messages
    public class IncomingReader implements Runnable { 
        public void run() {
            String message; 
            try {
                while ((message = reader.readLine()) != null) { 
                    System.out.println("read " + message); 
                    incoming.append(message + "\n");
                } // close while
            } catch(Exception ex) {ex.printStackTrace();}
        } // close run
    } // close inner class
}

class VerySimpleChatServer {
    ArrayList clientOutputStreams;

    public class ClientHandler implements Runnable { 
        BufferedReader reader;
        Socket sock;

        public ClientHandler(Socket clientSocket) { 
            try {
                sock = clientSocket;
                InputStreamReader isReader = new InputStreamReader(sock.getInputStream()); 
                reader = new BufferedReader(isReader);
            } catch(Exception ex) {ex.printStackTrace();} 
        } // close constructor

        public void run() {
            //uses tellEveryone() on your message
            String message;
            try {
                while ((message = reader.readLine()) != null) {
                    System.out.println("read " + message); 
                    tellEveryone(message);
                }
            } catch(Exception ex) {ex.printStackTrace();}
        }
    }

    public static void main (String[] args) {
        new VerySimpleChatServer().go();
    }

    public void go() {
        clientOutputStreams = new ArrayList(); 
        try {
            //connects to server and runs code
            ServerSocket serverSock = new ServerSocket(1985);
            while(true) {
                Socket clientSocket = serverSock.accept();
                PrintWriter writer = new PrintWriter(clientSocket.getOutputStream()); 
                clientOutputStreams.add(writer);
                Thread t = new Thread(new ClientHandler(clientSocket)); 
                t.start();
                System.out.println("got a connection");
            }
        } catch(Exception ex) {ex.printStackTrace();}
    } // close go

    public void tellEveryone(String message) {
        Iterator it = clientOutputStreams.iterator(); 
        while(it.hasNext()) {
            try {
                PrintWriter writer = (PrintWriter) it.next(); 
                writer.println(message);
                writer.flush();
            } catch(Exception ex) {
                ex.printStackTrace(); 
            } // end while
        }
    } // close tellEveryone 
} // close class
